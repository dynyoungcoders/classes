#!/bin/bash

# Just a shortcut script to clean out the directory and build the presentation

# In case path has spaces in the name
SAVE_IFS=$IFS
IFS=$(echo -en "\n\b")

# Move to where we're going to work
orig_path=$(pwd)
base_dir="$(dirname $0)"
cd "${base_dir}"

# Remove existing html files, just to be explicit
rm_glob="*html"
for reapable in ${rm_glob}; do
    rm -f "${reapable}"
done

# Rebuild the presentation
config_file="intro_with_rgkit.cfg"
time landslide -w "${config_file}"

cd ${orig_path}

# Reset token boundry
IFS=${SAVE_IFS}

