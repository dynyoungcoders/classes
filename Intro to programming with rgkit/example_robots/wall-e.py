import rg

class Robot:
    def act(self, game):
        # if we're in the center, stay put
        if self.location[0] == rg.CENTER_POINT[0]:
          # if there are enemies around, attack them
           for loc, bot in game.robots.iteritems():
              if bot.player_id != self.player_id:
                  if rg.dist(loc, self.location) <= 1:
                      return ['attack', loc]
        
        # move toward the center
        wallpoint = (rg.CENTER_POINT[0], self.location[1])

        test_choice = rg.toward(self.location, wallpoint)

        # if there's already a brick there aim lower
        if test_choice in game.robots:
             test_choice= rg.toward(self.location, (wallpoint[0], wallpoint[1] +1))

        if test_choice in game.robots:
            return['guard']
        else:
          return ['move', rg.toward(self.location, test_choice)]
