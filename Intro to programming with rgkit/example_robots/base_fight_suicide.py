# This robot picks a base location
# if we're already at our "base" we just guard
# then it looks for robots next to it
# if more than one robot is nearby we suicide
# if only one is nearby we attack it
# if nothing else to do, we march to the center
import rg

class Robot:
    def act(self, game):
        base = rg.CENTER_POINT # usually (9,9)

        # if we're in the center, guard
        if self.location == base:
            return ['guard']

        neighbors = 0

        # loop through all the robots
        for loc, bot in game.robots.iteritems():

            # if this is an enemy robot
            if bot.player_id != self.player_id:
            
                # if this robot is next to us
                if rg.dist(loc, self.location) <= 1:
                    neighbors += 1
                    attack_loc = loc

        # if multiple bad neighbors - suicide
        if neighbors >1:
            return['suicide']

        # if just one neighbor - attack them
        if neighbors:
            return ['attack', attack_loc]

        # move toward the center
        return ['move', rg.toward(self.location, base)]
