"""
This robot will do one of two things.

If there is a robot owned by the other player anywhere nearby, it will suicide
Otherwise, it will try to move one square towards the middle
"""

import rg


class Robot(object):
    """
    This 'class' has the code that will control all our robots.  Each robot
    will get a copy of this code.
    """

    def act(self, game):
        """
        This function gets called every time the robot gets a turn
        """

        # Look through all the robots on the board
        for loc, bot in game.robots.iteritems():

            # If it isn't one of ours...
            if bot.player_id != self.player_id:

                # And they're right next to us...
                if rg.dist(loc, self.location) <= 1:

                    # We commit suicide!  Boom!
                    return ['suicide']

        # Otherwise, we just move towards the center of the board
        return ['move', rg.toward(self.location, rg.CENTER_POINT)]
