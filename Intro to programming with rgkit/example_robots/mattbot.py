import random
import rg

class Robot:
    """
    Matt's awesome robot

    basically it just moves in a random direction
    """
    def act(self, game):

        return ['move', rg.toward(self.location, (3,3))]
