import rg

class Robot(object):
    def act(self, game):
        print rg.loc_types(self.location)
        # if we're on a spawn location - move towards the center
        if "spawn" in rg.loc_types(self.location):
           return ['move', rg.toward(self.location, rg.CENTER_POINT)] 

        # otherwise - just hangout
        return["guard"]

