import random
import rg
from rgkit import game

class Robot:
    def act(self, game):
        # find valid adjacent spaces
        adjacent = rg.locs_around(self.location,
                                  filter_out=('invalid', 'obstacle'))

        # ...containing enemies
        adjacent_enemies = [ loc for loc in adjacent
                             if game.robots.get(loc, self).player_id
                             != self.player_id ]

        # attack if we can
        if adjacent_enemies:
            return ['attack', random.choice(adjacent_enemies)]

        # move, ignoring friendly robots (since they will probably move too)
        return ['move', random.choice(adjacent)]
