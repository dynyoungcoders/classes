# Functions

----

# What *is* a function?
* A small program used by other programs
* A block of code in your program that does ONE thing, really well
* A way to let you NOT have to think of EVERYTHING

----

# Example of a function

    !python
    def say_hello():
        """Give a friendly greeting"""
        print("Hello there!")

----

# Using functions

    !python
    # First we DEFINE the function
    def say_hello():
        """Give a friendly greeting"""
        print("Hello there!")

    # Second we CALL the function
    say_hello()

----

# But *WAIT* I'm already USING functions!

* What functions have we used so far?
* Do you have a favorite function?
* What is your *dream* function?

# Presenter notes

functions we've used<br/>
randint()<br/>
str() / int()<br/>
print()<br/>
raw_input()<br/>
type()<br/>
range()<br/>
<br/>
What about *for* and *if* and *else*

----

# A tiny bit more useful function

    !python
    from random import randint

    def flip_coin():
        n = randint(0,1)
        if n==0:
            print "Heads"
        else:
            print "Tails"

    flip_coin()

----

# Paramaters

* Things you pass INTO a function
* Sometimes called *arguments*

This function has a parameter called name

    !python
    def say_hello(name):
        print("Hello "+ name)

This function has 2 parameters that we add together

    !python
    def add_numbers(x, y):
        z = x + y
        z_str = str(z)
        print("I added your numbers, the answer is "+ z_str)

    add_numbers(3, 5)

----


# Return Values

Functions can send stuff BACK to the code that called it

    !python
    def add_numbers(x, y):
        z = x + y
        return z

    result = add_numbers(3, 5)
    print result

# Presenter notes
Identify each variable in this slide
<ul>
  <li>x</li>
  <li>y</li>
  <li>z</li>
  <li>add_numbers</li>
</ul>

----

# Functions can be changed

  !python
  def say_hello(name):
      print("Hello "+ name)

  say_hello("Bob")

  def say_hello(name):
      print("Hola! " + name)

  say_hello("Bob")

-----

# Where INDENTATION ruins your day

    !python
    def count_to_number(x):
        for i in range(0, x):
        print i

----

# Functions LOVE to use other functions

    !python
    from random import randint

    def ate_pizza(name):
        p = randint(1,10)
        p_str = str(p)
        print(name + " ate " + p_str + " pieces of pizza")

    ate_pizza("Bob")

# Presenter Notes

Maybe fix the "piece" vs "pieces" problem

----

# Functions really WANT to be used over... and over... and over

    !python
    from random import randint

    def ate_pizza(name):
        p = randint(1,10)
        p_str = str(p)
        print(name + " ate " + p_str + " pieces of pizza")

    students = ('Adele', 'Ocean', 'Henning', 'Nick', 'Cole', 'M@')

    for student in students:
        ate_pizza(student)

----

# Functions LOVE to get a list

    !python
    def ate_pizza(people):
    for person in people:
        p_str = str(randint(1,10))
        print(person + " ate " + p_str + " pieces of pizza")

    ate_pizza( ('Adele', 'Ocean', 'Henning', 'Nick'))


----

# The parts of a function
    !python
    def say_hello(myname):
        print ‘Hello’, myname

<table>
    <tr>
        <td>def</td> <td> keyword that STARTS a function </td>
    </tr> <tr>
        <td>myname</td> <td> This is a parameter (and variable) </td>
     </tr> <tr>
        <td>say_hello</td> <td> This is the name of the function</td>
     </tr> <tr>
        <td>print....</td> <td> This is the body of the function</td>
    </tr>
</table>


