# Building our first game

----

# Creating a file with IDLE

We're going to create our first game, but first we're going to make sure that we can save it, run it, and change it again.

* Click on File->New File
* In the new window, add this line:

        !python
        #!/usr/bin/env python

* Click on File->Save (name it something ending in ".py")

# Presenter Notes

* Explain about saving the file.  Show how to run it (Hit F5)

----

# Number Guessing Game

Now we're going to build our first game together.  Be sure to save your code from time to time

# Presenter Notes

(Live code this)

    !python
    picked_number = 5

    guess = raw_input("Enter the number to guess: ")
    guessed_number = int(guess)

    if guessed_number == picked_number:
        print("You guessed correct!")
    else:
        print("Sorry, you guessed wrong.")

----

# Just a quick comment on comments

Sometimes it's hard to understand code when you go look at it later.  A good
rule to think about:

> Always code as if the person who will end up working on your code later is an
> axe wielding murderer who knows where you live.

In other words, making your code readable is a good idea.  Comments can help.
Here's an example:

    !python
    # This variable stores the number that the user has to guess
    picked_number = 5

    # Ask the user for a guess
    guess = raw_input("Enter the number to guess: ")

    # It will be a string at first, so we convert it to an integer
    guessed_number = int(guess)

# Presenter notes

Remind the kids that *they* might be the axe wielding murderers themselves

----

# Repeating ourselves. Repeating ourselves.

Sometimes, we have to do the same thing, over and over.  Like trying to print all the numbers from 1 to 100.

    !python
    a = 1
    print(a)
    a = a+1
    print(a)
    a = a+1
    print(a)

This is going to take a long time, and is kinda silly.  Computers are supposed to make our lives easier.

# Presenter Notes

Have the kids type this into a file, so we can revisit/edit it
Then, get them to use verbal instructions on how they would instruct somebody to do this,
key is to see if they say something like "do the same thing again"

----

# Loops - here to save us from going crazy

Python can handle repeating things for us, so that we don't have to type the same
stuff over and over.  Here's the first type of loop we'll look at:

    !python
    for a in range(1, 11):
        print(a)

The "range" function includes the first number but not the last one.
Sometimes computers are silly too.

# Presenter Notes

* Start with a smaller list, so the output fits on the screen.  Let them play around
  with making lists that are far too long
* We haven't covered lists/functions yet, so gloss over what range does
* Make note of the indentation

----

# Looping until something happens

Sometimes we want to keep doing something until it works right, instead of
doing it a certain number of times.

    !python
    stuff_typed_in = ""

    while stuff_typed_in != "yes":
        stuff_typed_in = raw_input("type 'yes' here: ")

    print("You did it!")

# Presenter Notes

Let the kids try this out and play with it

Make note of indentation again

----

# Making our game better!

Let's use loops so that they can keep guessing until they get the right
answer.

Computers may be stupid, but they're also really good at doing things over
and over and over and over and over ...

Don't forget to keep saving your work!

# Presenter Notes

Let them try before showing our solution

----

# How we added loops to the game

    !python
    picked_number = 5

    guess = raw_input("Enter the number to guess: ")

    while guessed_number != int(guess):
        print("Sorry, that's not correct.")
        guess = raw_input("Enter the number to guess: ")

    print("Woot!  That was the right answer!")

----

# This game is way too easy

The problem with this game is that it's *way* too easy.  We have to change
the code if we want a different number each time.  Where's the fun in that?

Meet "random"!  In the python window in IDLE (not in an open file), try this:

    !python
    >>> from random import randint
    >>> a = randint(1, 10)
    >>> print(a)
    # What did you get?
    >>> a = randint(1, 10)
    >>> print(a)
    # How about this time?

"randint" is a function - we'll learn more about these soon.

# Presenter notes

Give a quick explanation that randint is a command that gives us something
back.  By doing "a = randint...", we're taking what randint gives us back and
 are putting it in the variable "a".  Also make mention of libraries.

----

# Making our game more of a challenge

How do we update our game to make use of this?

# Presenter notes

Change to

    !python
    from random import randint
    picked_number = randint(1,10)

----

# What else can we do?

* Count the number of guesses it took to get the right answer
* Let the player choose the highest number
* Tell the player if their choice was too high or too low

# Presenter notes

* Let the kids pick one (or more) of these and implement it, with help if needed
