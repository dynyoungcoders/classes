
# RGKit coding

----

# Simple (and kinda stupid) bot, explained

    !python
    import rg

    class Robot(object):
        def act(self, game):
            # We're dumb.  We're just going to guard
            return ["guard"]

* import rg ... bringing in a library
* class Robot (this just has to be there)
* def act(self, game) ... the function that gets called each round for each robot
* return ... how we tell the arena what command our robot will be running

# Presenter Notes

* Can create our own functions
* First argument *must* be "self" ... just smile and nod
* Point out that the return is a list, and the first item is the actual command
* Explain that this code will be loaded onto every one of the robots

----

# What is a location?

Locations are defined as a tuple (x_coordinate, y_coordinate)

<img src="images/location_example.png" alt="Location Example" height="400" width="400" />

----

# Location types

For a robot, each location can have a couple "types":

* invalid: Something like (-1, 1) for example
* normal: Just a valid spot
* spawn: Where a bot could/would spawn
* obstacle: A location you can't go, even though it's valid

<img src="images/location_types.png" alt="Location Types" height="350" width="350" />

----

# Issuing a command

Just return a list with the command and any optional arguments:

    !python
    # Just guard
    return ["guard"]

    # Just suicide
    return ["suicide"]

    # Move to a square
    return ['move', (10, 14)]

    # Attack a square
    return ['attack', (10, 14)]

# Presenter Notes

* Target location must be valid
* The location must be a tuple nested in the list

----

# Robot, know thyself

Here's some stuff the robot can find out about itself:

* self.location: Contains a location tuple for "this" robot
* self.hp: How many health points "this" robot has left
* self.player_id: An id explaining what player "this" robot belongs to
* self.robot_id: An id for "this" specific robot

# Presenter Notes

* Talk about the dot between self and location, but gloss over it mostly
* Player id is just a number.  Sort of like getting a bib number at a race.  It will be the same for all your robots.
* Robot id is different for every robot

----

# Robot, know thy neighbors

Here's how a robot can know what the rest of the board looks like:

game.robots: A dictionary where the key is a location (tuple) and the value
  is another robot!

This code will print the location of all the robots on your team (including
yourself!)

    !python
    class Robot:
        def act(self, game):
            for loc, robot in game.robots.items():
                if robot.player_id == self.player_id:
                    print loc

# Presenter Notes

* Reiterate what dict.items() does
* Make sure they understand the difference between robot(.player_id) and
  self(.player_id)
* Note that this bot doesn't actually perform any actions

----

# Robot, know thy options

A robot can figure out what locations it could move to with this:

    !python
    # possible_locs will be a list of location tuples
    possible_locs = rg.locs_around(self.location)

We can also filter out types we don't want:

    !python
    # Now it will only have locations we can actually move to!
    possible_locs = rg.locs_around(
        self.location, filter_out=('invalid', 'obstacle')
    )

A reminder of location types: invalid, normal, spawn, obstacle

# Presenter Notes

* We haven't explained keyword arguments to functions - may have to explain
  that

----

# Robot, know what you're getting yourself into

If we want to know what's up with a location before we move there, we can use
game.robots to see if there's another robot there already:

    !python
    if target_location in game.robots:
        print "There's already a robot there!"
    else:
        print "Oh good - it's empty"

And we can know what "type" of location it is:

    !python
    # rg.loc_types returns a list of types associated with the location
    loc_types = rg.loc_types(target_location)
    print loc_types

# Presenter Notes

* Remind them that "blat in dict" checks to see if blat is a valid key in the
  dictionary
* We've seen game.robots before...
* Multiple types could be returned by rg.loc_types (normal and obstacle, for
  example)

----

# Robot, know where your center is

A handy variable that contains the location at the center of the board:

rg.CENTER_POINT

# Presenter Notes

Remind the kids that letter case matters

----

# Robot, know ... um ... random stuff

* rg.wdist(loc1, loc2): Tells how many moves it will take to get from loc1 to
  loc2
* rg.toward(loc1, loc2): Returns the next location in a path from loc1 to loc2

# Presenter Notes

* Purposefully didn't include rg.dist.  It gives Euclidian distance between
  points.  That's probably not useful to the kids at this point, as that
  doesn't describe how many moves it would take to get from loc1 to loc2.

----

# Robot, know that this is the end
