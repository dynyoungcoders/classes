# RGKit rules (finally!)

----

# Resources

Some helpful resources:

* [https://robotgame.net/](http://www.robotgame.net/)
* [http://www.reddit.com/r/robotgame](http://www.reddit.com/r/robotgame)
* [https://github.com/WhiteHalmos/rgkit](https://github.com/WhiteHalmos/rgkit)

----

# General rules

* You get 50 robots, released in waves (5 robots every 10 moves)
* Each robot starts with 50 health points
* Each robot gets to perform one action per round and there are 100 rounds in a game
* Each robot can look at the whole board, which doesn't count as an action
* You can't hurt your own robots, even by accident
* Winner is whichever team has more robots left at the end of 100 rounds

----

# Robot actions

These are the options a robot has for an action each round:

* Move
* Attack
* Suicide
* Guard

----

# Action: Move

* A robot can move exactly one square per round
* Colliding with enemy will damage your bot 5 points
* You can move straight, but not diagonal

<img src="images/robot_move.png" alt="Robot Move" height="200" width="200" />

----

# Action: Attack

* Can attack the same places that you can move
* If a robot is in the attacked square at the end of the turn, it will get 
8-10 points of damage

<img src="images/robot_attack.png" alt="Robot Attack" height="200" width="200" />

----

# Action: Suicide

* Robot explodes at the end of the turn
* Deals 15 damage to any robot next to it at the end of turn

----

# Action: Guard

* Robot doesn't move
* Takes only half damage from attacks and suicides
* Takes no damage from collisions
