
# Learning about dictionaries

----

# Looking something up

Lists are handy, but what if we wanted to look something up by name rather
than by an "index"?

For instance, what if we wanted to look up what weapon each of the TMNTs used?

<table>
    <tr><th>Ninja Turtle</th><th>Weapon</th>
    <tr><td>Leonardo</td><td>katana</td></tr>
    <tr><td>Michelangelo</td><td>nunchakus</td></tr>
    <tr><td>Donatello</td><td>bo staff</td></tr>
    <tr><td>Raphael</td><td>sai</td></tr>
</table>

Depending on who you talk to, this can be called a "map", a "hash", or (as in
Python) a "dictionary" (or a "dict" for short - programmers).

# Presenter notes

Let the kids come up with other examples of dicts

----

# How does Python do dictionaries?

Like this:

    !python
    >>> tmnt_weapons = {"Leonardo": "katana", "Michelangelo": "katana",
    "Donatello": "bo staff", "Raphael": "sai"}
    >>> print(tmnt_weapons)
    >>> print(tmnt_weapons["Leonardo"])
    >>> print(tmnt_weapons["Raphael"])
    >>> print(tmnt_weapons["Splinter"])

The thing you use to look something up is called a "key".

The thing you are looking up is called a "value".

----

# How are dictionaries different than lists?

* Lists have an order, dictionaries don't
* Lists always have a numeric index, dictionaries can have all different
kinds of indexes/keys (strings, floats, etc)

Which one would you use for:

* Instructions to drive somewhere?
* Keeping track of how many of each Pokemon card you have?
* Knowing what spell to cast
* Remembering what homework you have to do
* Organizing a Music Collection

# Presenter notes

Actually let the kids create these structures.  Let them try to stump each
other with other scenarios.

    !python
    {
        'Avada Kedavra' : 'Causes Instant, painless death',
        'Confundo' : 'Causes the vistim to become confused',
        'Alohomora' : 'Unlock doors',
    }

----

# How do I loop through a dictionary?

The *REALLY* important thing to remember is that dictionaries do not have an
order to them.  You don't know what order they might come back in.

    !python
    # Doctor Who characters and whether they're good or bad
    dw_characters = {
        'The Doctor': 'Good',
        'The Master': 'Bad',
        'River Song': 'Good',
        'Silence': 'Bad',
        'Mickey': 'Questionable',
    }

    # This will set char_name equal to each KEY in the dictionary as we go
    # through the loop
    for char_name in dw_characters:
        print(char_name + " is a " + dw_characters[char_name] + " character.")

----

# Getting all the keys or values

We can even get all the keys or all the values as a list:

    !python
    key_list = dw_characters.keys()
    print(key_list)

    value_list = dw_characters.values()
    print(value_list)

Or get the dictionary as a list of tuples:

    !python
    items = dw_characters.items()
    print(items)

----

# Nesting data structures

"Nesting" is an important idea.  It describes when we put a "data structure"
(something like a list or a dictionary) inside another data structure.  We
saw an example of this in the last slide:

    !python
    items = dw_characters.items()
    print(items)

In this case, we can see we have a list where each of the items in the list
are tuples.  This is a "nested data structure".

We can create whatever we want.  Here's a dictionary where the values are
lists:

    !python
    minecraft_inventory = {
        'matt': ['pickaxe', 'leather helmet', 'diamond'],
        'cole': ['dirt blocks', 'more dirt blocks', 'even MORE dirt blocks'],
    }

# Presenter notes

Make sure they understand that "nested data structure" is just a phrase to
describe this sort of thing -  nothing more.  There's nothing special or
magical about it.