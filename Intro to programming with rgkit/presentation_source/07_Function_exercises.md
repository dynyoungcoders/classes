# Function Exercises
## (but without the sweat)

----

# General instructions

These exercises are designed to help you try out some of the stuff we have
learned, and to help Cole and Matt know if there are any area we need to work
on before we move on to working with rgkit.

Write each of these functions and save them all in a single file. Remember to
give the functions names that won't cause an axe wielding murderer to show up
to your house.

----

# Exercise 1: Simple subtraction

Write a function that will take two arguments.  The function should subtract
the *SECOND* argument from the *FIRST* argument and return that result.

----

# Exercise 1: Our solution

    !python
    def subtract(first, second):
        return first - second

----

# Exercise 2: Word counts

Write a function that will take a single argument.  This argument will be a
list of words.  Return a dictionary that gives a count of how many times each
word shows up in the list.

A hint: You can create a default value in a dictionary by doing this:

    !python
    my_dict = {}
    my_dict.setdefault("key", 0)
    print(my_dict["key"])
    
    # Doesn't change a key that already exists
    my_dict["key"] = 1000000
    my_dict.setdefault("key", 0)
    print(my_dict["key"])

----

# Exercise 2: A step in the right direction...

    !python
    def word_count(word_list):
        counts = {}

        for word in word_list:
            # for now - just print out each word
            print word

        return counts


----

# Exercise 2: Our solution

    !python
    def word_count(word_list):
        counts = {}
        
        for word in word_list:
            # Here's a good trick to use ... we'll set it to 0 if it's not 
            # already set to something
            counts.setdefault(word, 0)
            counts[word] += 1
            
        return counts
        
----

# Exercise 3: Choose from a menu

Write a function that takes no arguments.  It will print out a list of four
things (you get to choose what), with a number next to each.  The function 
should ask the user to pick one thing from the list, and return the number (as 
an integer, not a string) that the user picked.  

A hint:

    !python
    test_list = ["item", "another item", "yet another item"]
    
    for index, value in enumerate(test_list):
        print(index + ": " + value)

----

# Exercise 3: Our solution

    !python
    def get_menu_choice():
        menu_options = ["Snails", "Frog Legs", "Fish Eggs", "Blue Cheese"]
        print("Here are your choices:")
        for index, option in enumerate(menu_options):
            print(str(index) + ": " + option)
        
        choice = raw_input("Which one would you like: ")
        return int(choice)

----

# Exercise 4: Number check

Write a function that takes two arguments.  Return True if either argument is
10, or if the two arguments added together is 10.  Otherwise, return False.

----

# Exercise 4: Our solution

    !python
    def check_for_10(first, second):
        if first == 10:
            return True

        if second == 10:
            return True
            
        if first + second == 10:
            return True
            
        return False
        
----

# Exercise 5: Alarm clock

We're working on a function for an alarm clock.  This alarm clock will know
whether to turn the alarm on based on whether what day of the week it is, and
if we are on vacation.  The function should take two arguments.  The first one
will be a boolean to tell the function if it's a weekday (Monday-Friday) or
not.  The second argument will be a boolean to tell the function if we're on
vacation or not.  The result will be whether or not the alarm should be turned
on.  The rules is this: if we are not on vacation, and it is a weekday, the
function should return True.  Otherwise, we don't want to have to get up, and
the function should return False.

----

# Exercise 5: Our solution

    !python
    def have_to_wake_up(is_weekday, is_vacation):
        # We're on vacation?  No WAY we're waking up!
        if is_vacation:
            return False
            
        # Ugh, it's a weekday and we're not on vacation.  Guess we have to 
        # Wake up.
        if is_weekday:
            return True
            
        # Weekend time!  No alarm and sleep in!
        return False

----

# Exercise 6: Reverse a list

Write a function that accepts a list as its only argument and returns a list
that has all the values from the first list but in the opposite order.  That
is, if we passed in a list that looked like 

    !python
    ['a', 'b', 3, 'ice cream']
    
the returned list should be

    !python
    ['ice cream', 3, 'b', 'a']
     
A couple hints: 

- You can use the "len" function to find out how long a list is.
- The following code will create numbers from 10 down to 0: range(10, -1, -1)
- Doing this will add an element to the end of a list: list_name.append(element)

----

# Exercise 6: Our solution

    !python
    
    def reverse_list(a_list):
        new_list = []
        list_length = len(a_list) -1
        
        for index in range(list_length, -1, -1):
            new_list.append(a_list[index])
            
        return new_list
        
----

# Exercise 6: Even better solution

    !python
    
    def reverse_list(a_list):
        # Make a copy of the list
        new_list = a_list[:]
        new_list.reverse()
        
        return new_list
        
----

# Exercise 7: Is it prime?

Write a function that takes an integer and returns a boolean as to whether or
not that number is a prime number.  A prime number is a number that can only be
divided evenly by itself and "1".  A hint: The modulo operator (which is a '%'
in Python) will tell you if there is a remainder when you divide.  For instance,
if you do 6 % 3 in Python, it will come back with 0 because 6 can be evenly
divided by 3.  However, if you do 5 % 3, it will return 2 because 3 goes into 5
just once, with 2 left over.

----

# Exercise 7: Our solution

    !python
    
    def is_prime(num):

        for factor in xrange(2, num - 1):
            if num % factor == 0:
                return False
        
        return True
