# Getting Started

----

# What *is* a program anyway?

* Computers are stupid
* No, I mean really *really* stupid
* They can't figure things out on their own
* They have to be told how to do *EVERYTHING* (sort of like your younger brother/sister)

----

# Let's get started!

* Start up your system
* Username: student  Password: student

# Presenter Notes

* Help the kids get booted up/logged in

----
# Start up idle

![](images/menu.png)

* Click on the menu button, bottom left corner (picture above shows where to click)
* Move the mouse up to "Programming"
* Click on "IDLE"

----


# Hello World

    !python
    >>> print("hello, world")
    hello, world

# Presenter Notes

* "Note the double quotes - we'll explain why those are there later"

----

# What's your name

    !python
    >>> n = raw_input("What is your name? ")
    # What happens here?

    >>> print("Hello" + n)
    # What do you think will happen here?

# Presenter Notes

* A variable is just a "box" to hold things.  The box has a name, but it can hold all kinds of different stuff

----

# Integers (also known as ints)

    !python
    >>> a = 3
    >>> print(a)
    # What happens here?
    >>> type(a)
    # What on earth is this?

    >>> a = 17
    >>> print(a)
    # I bet you can guess what this will do...
    >>> type(a)
    # Hmmm - I see a pattern here...

    >>> a = 897234789234789324879234978234789324
    >>> print(a)
    # Yup - we can even do *really* big numbers
    >>> type(a)
    # What do you think this will show us?

# Presenter Notes

* ints are just whole numbers
* longs are just really big ints - they won't need to know when it's a long or an int, but good to know that both exist

----

# Strings

    !python
    >>> a = 3
    >>> type(a)
    # What type is a?

    >>> b = "Hello World"
    >>> type(b)
    # What type is b?

    >>> c = "3"
    >>> type(c)
    # Is c the same as a?

    >>> a + b
    # What happens when you try this?

    >>> a + c
    # Discuss!

    >>> print a
    >>> print c
    >>> a == c
    # What happened?

# Presenter Notes

Class one has not seen this slide

----

# Conditions

Sometimes you need to have the computer make decisions.  And since the computer is stupid, we have to be really clear about what it needs to do.

    !python
    a = 5

    if a == 5:
        print("Duh, of course it's 5.")
    else:
        print("No, it's not 5.  It's" + a)

# Presenter Notes

* Point out the == and explain the difference between it and the =
* Don't forget the ':' at the end of the if block