# Intro to programming with rgkit

# Presenter Notes

* Let the kids introduce themselves

----

# Who *are* these bearded guys?

<table>
    <tr>
        <td><img src="images/ColeTuininga.jpg" alt="(Cole Tuininga)"  height="210" /></td>
        <td><img src="images/MattSheppard.png" alt="(Matt Sheppard)"  height="210" /></td>
    </tr>
</table>

# Presenter Notes

* Cole and Matt introduce themselves
* Talk about what we do

----

# Python and rgkit

Python is a programming language.  It's one of many, but it's the one we're going to use.

Rgkit is "Robot Game Kit".  [http://www.robotgame.net/](http://www.robotgame.net/)

# Presenter Notes

* Python - it's a *real* language, used for real work
* rgkit - library (define that term) for writing programs to compete in the robot game arena

----

# Demo of rgkit

----

# Sample rgkit Robot code

    !python
    import random
    import rg

    class Robot:
        """Code for controlling a very simple robot."""
        def act(self, game):
            """Called for every round in the game"""

            # Find out where we can move
            possible = rg.locs_around(self.location,
                filter_out=('invalid', 'obstacle'))

            # Pick a random item from the list
            return ['move', random.choice(possible)]

----

# What we're doing today
