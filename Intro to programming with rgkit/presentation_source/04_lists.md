
# Learning about Lists

----

# Lists

Lists in real life:

* Grocery lists
* TODO lists
* Camping lists
* Directions to get somewhere

Really, a list is just a bunch of stuff all kept together, in order

# Presenter notes

Let the kids come up with any other lists.  Talk about whether the order
matters, and if we know how many items are in the list.

----

# Lists and indexes

Python list of ores:

    !python
    >>> ores = ['Diamond', 'Iron', 'Redstone']
    >>> print(ores)
    >>> print(ores[0]) # this is a number zero, not a letter "oh"
    >>> print(ores[1])
    >>> print(ores[2])
    >>> print(ores[3])
    # You can even use a variable
    >>> index = 0
    >>> print(ores[index])

<table>
    <tr>
        <th>Index</th>
        <th>Value</th>
    </tr>
    <tr>
        <td>0</td>
        <td>Diamond</td>
    </tr>
    <tr>
        <td>1</td>
        <td>Iron</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Redstone</td>
    </tr>
</table>

# Presenter notes

Don't get into negative indexes or slicing just yet - we'll hit that in a
minute

----

# Lists and negative indexes

We can get the items from the list, coming from the end too!

    !python
    >>> ores = ['Diamond', 'Iron', 'Redstone']
    >>> print(ores[-1])
    >>> print(ores[-2])
    >>> print(ores[-3])

<table>
    <tr>
        <th>Index</th>
        <th>Value</th>
    </tr>
    <tr>
        <td>-3</td>
        <td>Diamond</td>
    </tr>
    <tr>
        <td>-2</td>
        <td>Iron</td>
    </tr>
    <tr>
        <td>-1</td>
        <td>Redstone</td>
    </tr>
</table>

----

# List slicing

Sometimes we want **part** of a list.  This is called "slicing".

    !python
    >>> mobs = ['zombie', 'creeper', 'skeleton', 'enderman', 'spider']

    # We know what this one will do...
    >>> mobs[0]

    # We even know what this will do...
    >>> mobs[-1]

    # What about these? (This is the "slicing")
    >>> print(mobs[0:2])
    >>> print(mobs[2:4])
    >>> print(mobs[0:7])
    >>> print(mobs[0:-2])

# Presenter notes

See if the kids can figure out what the rules are for the two fields in the
slices.  Give them the hint that it works like "range" which we saw before.

----

# Looping with a list

If we want to do something with each item in a list, it would look like this:

    !python
    for variable_name in list_name:
        # do something with variable_name

Here's an example:

    !python
    groceries = ['chocolate milk', 'cake', 'ice cream', 'chips', 'tums']

    for item in groceries:
        if item == 'tums':
            print("Buy this for mom and dad: " + item)
        else:
            print("Buy this for me: " + item)


Using slicing, how could we change this code to loop on all the items except
the last one?

----

# Tuples

Tuples are a lot like lists, but you can't change them once you create them

    !python
    chore_tuple = ('trash', 'feed the dog', 'clean the car')
    chore_list = ['trash', 'feed the dog', 'clean the car']

    chore_list[0] = 'play video games'
    chore_tuple[0] = 'play video games'

    print(chore_list)
    print(chore_tuple)

# Presenter notes

Also make mention of the syntactic "oddness" of creating a single element tuple
