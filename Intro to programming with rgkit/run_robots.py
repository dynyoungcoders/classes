import os
from subprocess import call, check_output

path = 'robot_arena/'
robots = os.listdir(path)
scoreboard = {}

# for a countdown - calculate how many battles were about to start
total_battles = len(robots)**2

for robot1 in robots:
    scoreboard[robot1] = 0
    for robot2 in robots:

        print(total_battles),
        total_battles -=1

        if not robot2 in scoreboard:
            scoreboard[robot2] = 0

        # Call rgrun with earch robot, with verbosity low
        result = check_output(["rgrun","-H","-qqq",path + robot1, path + robot2])

        # parse scores out of return... not a pretty parse
        score1, score2 = result.split('[')[1].split(']')[0].split(',')

        # We don't count ties
        if int(score1) > int(score2):
            scoreboard[robot1] += 1
        else:
            scoreboard[robot2] += 1

print('\nResults')
for score in sorted(scoreboard, key = scoreboard.get, reverse=True):
    print score, scoreboard[score]



